<?php if($data): ?>
    <?php foreach ($data['items'] as $todo): ?>
        <div class="card-container col-md-4">
            <div class="card grow clearfix">
                <div class="card-body">
                    <h2 class="card-title
                            <?php if($todo['done']): ?>
                            done
                            <?php endif; ?>
                        ">
                            <?= $todo['body'] ?>
                    </h2>
                </div>
                <div class="float-rt">
                    <span class="card-author"><?= $todo['name']?></span>
                    <span class="card-author"><?= $todo['email']?></span>
                    <img class="media-img" src="images/<?= $todo['picture']?>" alt="car" width="160"/>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="clearfix"></div>
    <div class="pagination">
        <a class="<?php if((int)$_GET['page'] <= 1): ?>
        disabled
        <?php endif; ?>"
           href="?page=<?php if((int)$_GET['page'] > 1) echo (int)$_GET['page'] - 1?>"
        >&laquo;
        </a>
            <?php for($i=1; $i<=$data['allPages']; $i++): ?>
                <a class="
                <?php if((int)$_GET['page'] === $i): ?>
                active
                <?php endif; ?>
                " href="?page=<?= $i ?>"> <?= $i ?></a>
            <?php endfor; ?>
        <a class="<?php if(empty($_GET['page']) || (int)$_GET['page'] >= $i-1): ?>
        disabled
        <?php endif; ?>"
           href="?page=<?php if((int)$_GET['page'] < $i-1) echo (int)$_GET['page'] + 1?>"
        >&raquo;
        </a>
    </div>
<?php endif; ?>
<h3 class="text-center">Add new todo</h3>
<div class="form-block">
    <form enctype="multipart/form-data" action="main/createTodo" method="post">
        <div class="controls">
            <label class="control-label">Name</label>
            <input class="form-control" id="input-name" name="name" placeholder="Name" required>
            <label class="control-label">Your Email</label>
            <input class="form-control" id="input-email" name="email" type="email" placeholder="Email" required>
            <label class="control-label">New Todo</label>
            <input class="form-control" id="input-body" name="body" placeholder="Type a new wish" required>
            <label class="control-label">Add a Picture</label>
            <div><input class="form-control" type="file" id="image" name="image" accept="image/jpeg, image/gif, image/png" required></div>
            <input id="reset" type="reset" value="Reset">
            <input id="submit" type="submit" value="Submit">
        </div>
    </form>
    <button class="preview-button" id="preview-button">Preview</button>
</div>
<div id="preview" class="preview">
    <div class="card-container">
        <div class="card clearfix">
            <div class="float-rt">
                <span id="preview-name" class="card-author"></span>
                <span id="preview-email" class="card-author"></span>
                <div><img alt="" id="image_preview" src="https://lh6.googleusercontent.com/-L17_eJqIsDI/UzbeiYk96RI/AAAAAAAASP4/SbOqhSuEyU4/s100-no/image-is-not-uploaded.jpg" width="160"/></div>
            </div>
            <div class="card-body">
                <h2 id="preview-body" class="card-title"></h2>
            </div>
        </div>
    </div>
</div>
