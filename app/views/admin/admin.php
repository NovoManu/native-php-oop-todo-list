<a href="<?= $baseUrl ?>admin/logout"><button class="logout">LogoUt</button></a>
<div class="todo-list">
    <?php if($data): ?>
        <?php foreach ($data['items'] as $todo): ?>
            <ul>
                <li class="
                        <?php if($todo['done']): ?>
                        done
                        <?php endif; ?>
                    ">
                    <span>
                    <?= $todo['body'] ?> by <?= $todo['name'] ?></span>

                    <div class="icons-change">
                        <a class="close" href="<?= $baseUrl ?>admin/delete?id=<?= $todo['id'] ?>" title="Delete">&times;</a>
                        <a class="close" href="<?= $baseUrl ?>admin/markAsDone?id=<?= $todo['id'] ?>" title="Mark as done">&#10004;</a>

                    </div>
                    <div class="update-todo" title="Edit todo">
                        &#9998;
                        <form action="<?= $baseUrl ?>admin/update?id=<?= $todo['id'] ?>" method="post">
                            <input name="body" value="<?= $todo['body'] ?>">
                            <button class="edit" type="submit">&#10004;</button>
                        </form>
                    </div>
                </li>
            </ul>
        <?php endforeach; ?>
        <div class="clearfix"></div>
        <div class="pagination">
            <a class="<?php if((int)$_GET['page'] <= 1): ?>
        disabled
        <?php endif; ?>"
               href="?page=<?php if((int)$_GET['page'] > 1) echo (int)$_GET['page'] - 1?>"
            >&laquo;
            </a>
            <?php for($i=1; $i<=$data['allPages']; $i++): ?>
                <a class="
                <?php if((int)$_GET['page'] === $i): ?>
                active
                <?php endif; ?>
                " href="?page=<?= $i ?>"> <?= $i ?></a>
            <?php endfor; ?>
            <a class="<?php if(empty($_GET['page']) || (int)$_GET['page'] >= $i-1): ?>
        disabled
        <?php endif; ?>"
               href="?page=<?php if((int)$_GET['page'] < $i-1) echo (int)$_GET['page'] + 1?>"
            >&raquo;
            </a>
        </div>
    <?php endif; ?>
</div>