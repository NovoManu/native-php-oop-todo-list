<?php

require_once ('app/core/Controller.php');
require_once ('app/models/Todo.php');
require_once ('app/models/Config.php');

class controller_main extends Controller
{
    public function index(){
        $todos = Todo::get(3);
        $this->view->createView("public/main.php", 'template.php', $todos);
    }

    public function createTodo(){
        $url = Config::get('app_url');
        if($_FILES['image']['name']){
            if(is_uploaded_file($_FILES['image']['tmp_name'])) {
                $sourcePath = $_FILES['image']['tmp_name'];
                $targetPath = "images/".$_FILES['image']['name'];
                move_uploaded_file($sourcePath,$targetPath);
                $picture = $_FILES['image']['name'];
            }
        } else $picture = 'default';
        $name    = htmlspecialchars(trim($_POST['name']));
        $email   = htmlspecialchars(trim($_POST['email']));
        $body    = htmlspecialchars(trim($_POST['body']));
        Todo::create($name, $email, $body, $picture);
        header('Location: ' . $url);
        exit();
    }
}