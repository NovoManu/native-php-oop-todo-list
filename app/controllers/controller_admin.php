<?php

require_once ('app/core/Controller.php');
require_once ('app/models/Todo.php');
require_once ('app/models/Admin.php');

class controller_admin extends Controller
{

    public function __construct(){
        parent::__construct();
        $url = Config::get('app_url');
        if(empty($_SESSION['logged']) && $_SERVER['REQUEST_URI'] != $url . 'admin/login'){
            var_dump(empty($_SESSION['logged']));
            header('Location: ' . $url . 'admin/login');
            exit();
        }
    }

    public function index(){
        $todos = Todo::get(5);
        $this->view->createView("admin/admin.php", 'template.php', $todos);
    }

    public function login(){
        $this->view->createView('admin/login.php', 'template.php');
    }

    public function checkLogin(){
        $url = Config::get('app_url');
        $name        = htmlspecialchars(trim($_POST['name']));
        $password    = htmlspecialchars(trim($_POST['password']));
        $loggedIn = Admin::checkLogin($name, $password);
        if($loggedIn){
            $_SESSION['logged'] = true;
        }
        header('Location: ' . $url . 'admin');
        exit();
    }

    public function logout(){
        $url = Config::get('app_url');
        $_SESSION['logged'] = false;
        header('Location: ' . $url . 'admin');
        exit();
    }

    public function markAsDone(){
        $url = Config::get('app_url');
        $id = $_GET['id'];
        if($id){
            Todo::markAsDone($id);
        }
        header('Location: ' . $url . 'admin');
        exit();
    }

    public function update(){
        $url  = Config::get('app_url');
        $id   = $_GET['id'];
        $body = htmlspecialchars(trim($_POST['body']));
        if($id){
            Todo::update($id, $body);
        }
        header('Location: ' . $url . 'admin');
        exit();
    }

    public function delete(){
        $url  = Config::get('app_url');
        $id   = $_GET['id'];
        if($id){
            Todo::delete($id);
        }
        header('Location: ' . $url . 'admin');
        exit();
    }
}