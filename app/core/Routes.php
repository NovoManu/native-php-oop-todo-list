<?php

require_once ('app/models/Config.php');

class Route
{
    static function start()
    {
        // Route by default
        $controllerName = 'main';
        $actionName = 'index';

        // Remove get params from URI
        $routes = explode('?', $_SERVER['REQUEST_URI']);
        // Explore controller and action by slicing URI
        $routes = explode('/', $routes[0]);

        if(!empty($routes[2])){
            $controllerName = $routes[2];
        }

        if(!empty($routes[3])){
            $actionName = $routes[3];
        }

        $modelName = 'Model_' . $controllerName;
        $controllerName = 'Controller_' . $controllerName;

        $modelFile = strtolower($modelName) . '.php';
        $modelPath = 'app/models/' . $modelFile;

        if(file_exists($modelPath)){
            include ($modelPath);
        }

        // Controller setup
        $controllerFile = strtolower($controllerName) . '.php';
        $controllerPath = 'app/controllers/' . $controllerFile;

        if(file_exists($controllerPath)){
            include ($controllerPath);
        }
        else{
            Route::Error404();
        }

        // Action setup
        $controller = new $controllerName;
        $action = $actionName;

        if(method_exists($controller, $action)){
            $controller->$action();
        }
        else{
            Route::Error404();
        }
    }

    public function Error404(){
        $url = Config::get('app_url');
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'. $url .'404');
    }
}