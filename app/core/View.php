<?php

require_once ('app/models/Config.php');

class View
{
    public function createView($contentView, $templateView, $data = null) {
        $baseUrl = Config::get('app_url');
        include ('app/views/' . $templateView);
    }
}