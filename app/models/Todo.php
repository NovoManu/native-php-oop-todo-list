<?php

require_once ('app/models/Database.php');
require_once ('app/core/Model.php');

class Todo extends Model
{
    static public function get($paginate = null){
        $db = new Database();
        return $db->getRows('SELECT SQL_CALC_FOUND_ROWS * FROM todos', $paginate);
    }

    static public function create($name = null, $email = null, $body = null, $picture = null){
        $db = new Database();
        return $db->insert("
              INSERT INTO todos (name, email, body, picture) 
              VALUES (:name, :email, :body, :picture)
              ",
            [
                'name'    => $name,
                'email'   => $email,
                'body'    => $body,
                'picture' => $picture,
            ]);
    }

    static public function update($id, $body = null){
        $db = new Database();
        $db->update("UPDATE todos SET body=:body WHERE id=:id", ['body' => $body, 'id'=>$id]);
    }

    static public function delete($id){
        $db = new Database();
        $db->delete("DELETE FROM todos WHERE id=:id", ['id'=>$id]);
    }

    static public function markAsDone($id){
        $db = new Database();
        $db->update("UPDATE todos SET done=1 WHERE id=:id", ['id' => $id,]);
    }
}