<?php

require_once ('app/models/Database.php');
require_once ('app/core/Model.php');

class Admin
{
    static public function checkLogin($name, $password){
        $db = new Database();
        $result = $db->getRow(
            'SELECT * FROM admins 
                  WHERE name=:name 
                  AND password=:password',
                [
                    'name'     => $name,
                    'password' => $password
                ]
            );
        if($result){
            return true;
        }
        return false;
    }
}