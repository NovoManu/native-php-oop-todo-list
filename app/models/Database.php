<?php

require_once ('app/models/Config.php');

class Database
{
    private $host;
    private $user;
    private $pass;
    private $dbname;
    private $isConnected;
    private $db;

    // Connect to database
    function __construct($options = []) {
        $config = Config::get('mysql');
        $this->host   = $config['host'];
        $this->user   = $config['username'];
        $this->pass   = $config['password'];
        $this->dbname = $config['db'];
        $dns = "mysql:dbname={$this->dbname};host={$this->host};charset=utf8";
        $this->isConnected = true;
        try{
            $this->db = new PDO($dns, $this->user, $this->pass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        }catch (PDOException $e){
            throw new Exception($e->getMessage());
        }
    }

    // Disconect from database
    public function disconnect(){
        $this->db = null;
        $this->isConnected = false;
    }

    // Get one row from database
    public function getRow($query, $params = []){
        try {
            $statement = $this->db->prepare($query);
            $statement->execute($params);
            $this->disconnect();
            return $statement->fetch();
        } catch(PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Get many rows from database
    public function getRows($query, $paginate = null, $params = []){
        // Pagination
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $start = ($page > 1) ? ($page * $paginate) - $paginate : 0;
        try {
            ($paginate === null) ?
                $statement = $this->db->prepare($query) :
                $statement = $this->db->prepare($query . " LIMIT {$start}, {$paginate}");
            $statement->execute($params);
            $total = $this->db->query("SELECT FOUND_ROWS() as total")->fetch()['total'];
            $allPages = ceil($total / $paginate);
            $this->disconnect();
            return [
                'items'    => $statement->fetchAll(),
                'allPages' => $allPages];
        } catch(PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Insert row into database
    public function insert($query, $params = []){
        try {
            $statement = $this->db->prepare($query);
            $statement->execute($params);
            $this->disconnect();
            return true;
        } catch(PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Update row in database
    public function update($query, $params = []){
        $this->insert($query, $params);
    }

    // Delete row from database
    public function delete($query, $params = []){
        $this->insert($query, $params);
    }
}