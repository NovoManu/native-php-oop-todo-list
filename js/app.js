$(document).ready(()=>{

    function removeUnsupportedFile() {
        $('#image').val('');
        $('#image_preview').attr('src', 'https://lh6.googleusercontent.com/-L17_eJqIsDI/UzbeiYk96RI/AAAAAAAASP4/SbOqhSuEyU4/s100-no/image-is-not-uploaded.jpg');
    }

    $('#image').change(function() {
        let input = $(this)[0];
        if ( input.files && input.files[0] ) {
            if ( input.files[0].type.match(/.(jpg|jpeg|png|gif)$/i) ) {
                let reader = new FileReader();
                reader.onload = function(e) {
                    $('#image_preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                removeUnsupportedFile();
                console.log('is not image mime type');
            }
        } else console.log('not isset files data or files API not suported');
    });

    $('#reset').on('click', ()=>{
        $('#image_preview').attr('src', 'https://lh6.googleusercontent.com/-L17_eJqIsDI/UzbeiYk96RI/AAAAAAAASP4/SbOqhSuEyU4/s100-no/image-is-not-uploaded.jpg');
    })

    $('#preview-button').on('click', ()=>{
        $('#preview').toggle(500);
        $('#preview-name').text($('#input-name').val());
        $('#preview-email').text($('#input-email').val());
        $('#preview-body').text($('#input-body').val());
    })

    $('.update-todo').on('click', (e)=>{
        let target = $(e.target);
        target.children().toggle();
    })
});
